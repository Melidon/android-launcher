package hu.bme.aut.android.launcher.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.fragment.app.Fragment
import hu.bme.aut.android.launcher.databinding.FragmentDialerBinding

class DialerFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentDialerBinding;

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDialerBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        for(child in binding.root.children) {
            child.setOnClickListener(this)
        }

        binding.btnCallBackSpace.setOnClickListener(this)
        binding.btnDialer1.setOnClickListener(this)
        binding.btnDialer2.setOnClickListener(this)
        binding.btnDialer3.setOnClickListener(this)
        binding.btnDialer4.setOnClickListener(this)
        binding.btnDialer5.setOnClickListener(this)
        binding.btnDialer6.setOnClickListener(this)
        binding.btnDialer7.setOnClickListener(this)
        binding.btnDialer8.setOnClickListener(this)
        binding.btnDialer9.setOnClickListener(this)
        binding.btnDialerStar.setOnClickListener(this)
        binding.btnDialer0.setOnClickListener(this)
        binding.btnDialerHashmark.setOnClickListener(this)
        binding.btnCall.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            binding.btnCallBackSpace.id -> {
                binding.etCall.setText(binding.etCall.text.dropLast(1))
            }
            binding.btnDialer1.id -> {
                binding.etCall.text.append('1')
            }
            binding.btnDialer2.id -> {
                binding.etCall.text.append('2')
            }
            binding.btnDialer3.id -> {
                binding.etCall.text.append('3')
            }
            binding.btnDialer4.id -> {
                binding.etCall.text.append('4')
            }
            binding.btnDialer5.id -> {
                binding.etCall.text.append('5')
            }
            binding.btnDialer6.id -> {
                binding.etCall.text.append('6')
            }
            binding.btnDialer7.id -> {
                binding.etCall.text.append('7')
            }
            binding.btnDialer8.id -> {
                binding.etCall.text.append('8')
            }
            binding.btnDialer9.id -> {
                binding.etCall.text.append('9')
            }
            binding.btnDialerStar.id -> {
                binding.etCall.text.append('*')
            }
            binding.btnDialer0.id -> {
                binding.etCall.text.append('0')
            }
            binding.btnDialerHashmark.id -> {
                binding.etCall.text.append('#')
            }
            binding.btnCall.id -> {
                val phoneNumber = "tel:" + binding.etCall.text.toString()
                val intent = Intent(Intent.ACTION_DIAL, Uri.parse(phoneNumber))
                requireContext().startActivity(intent)
            }
        }
    }
}
